var gulp = require('gulp');
var inject = require('gulp-inject');
var exec = require('exec');

//setup task for building the index file
gulp.task('index', function() {
    var target = gulp.src('./templates/index.html');
    // It's not necessary to read the files (will speed up things), we're only after their paths: 
    var sources = gulp.src(['./public/**/*.js', './public/**/*.css'], {
        read: false
    });
    //insert js/css dependencies in index.html and move template to public folder.
    //ignore any path references to public folder. without this it'll leave the public folder
    //name in the file path and files will not be served.
    return target.pipe(inject(sources, {
        ignorePath: 'public/'
    })).pipe(gulp.dest('./public'));
});

//gulp task to builds final index.htm with injects and activates local server
gulp.task('serve', ['index'], function(cb) {
    exec('node server.js', function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});
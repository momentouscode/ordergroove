(function (win) {

    if (!window.orderGroove) {
        window.orderGroove = {};
    }
    
    // handle jsonp calls
    function jsonp(url, params, callback) {
        var callbackName = 'jsonp_callback_' + new Date().getTime();
        window[callbackName] = function (data) {
            delete window[callbackName];
            document.body.removeChild(script);
            callback(data);
        };

        var script = document.createElement('script');
        script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
        document.body.appendChild(script);
    }
	
    //test of jsonp call. appears successful. 
    jsonp('https://openapi.etsy.com/v2/users/kemarcockburn.js?api_key=wyalj0b7h63wlpyr1nrqce6z', function (data) {
        if (data.ok) {
            // do something with the data here
            console.log('ok');
            console.log(data);
        } else {
            console.log('not ok');
            console.log(data.error);
        }
    });
})(window);